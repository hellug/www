---
title: "Meetups"
menu:
    main:
        parent: "draseis"
        weight: 2
---

Ο σύλλογος διοργανώνει τακτικές κοινωνικές συναντήσεις (meetups).

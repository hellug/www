---
title: "Εκδηλώσεις"
menu:
    main:
        parent: "draseis"
        weight: 2
---

Στην σελίδα αυτή ο σύλλογος κοινοποιεί τις εκδηλώσεις που συμμετέχει. Μια λίστα με τις μελλοντικές εκδηλώσεις
μπορείτε να βρείτε και στο [Hellug Wiki](https://wiki.hellug.gr/index.php?title=%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1:%CE%95%CE%BA%CE%B4%CE%B7%CE%BB%CF%8E%CF%83%CE%B5%CE%B9%CF%82).

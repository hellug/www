---
title: "Υπηρεσίες για Μέλη"
layout: "pages"
bannertemplate: "hellug-banner.html"
date: 2013-04-28
category_title: Μέλη
tags: [Υπηρεσίες για Μέλη]
meli: [ypiresies-gia-meli]
menu:
    main:
        parent: "meli"
        weight: 3
---
Ο Σύλλογος διαθέτει τις παρακάτω υπηρεσίες στα ενεργά μέλη του.
#### E-Mail
Παρέχεται η δυνατότητα email διεύθυνσης της μορφής: **username@hellug.gr**

#### WebSite
Χώρος για προσωπική ιστοσελίδα της μορφής: **members.hellug.gr/username/**

#### Collaborative Development
Υπηρεσία για hosting και συνεργατική ανάπτυξη λογισμικού και άλλου πηγαίου περιεχομένου
στο https://dev.hellug.gr

#### Matrix Chat Server
Υπηρεσία Διαδικτυακών Συνομιλιών και Συναντήσεων βασισμένο στο [matrix].
Τα μέλη μπορούν να εγγράφονται στο https://im.hellug.gr για λογαριασμό hellug.gr και όλοι μπορούν να συμμετέχουν στα δωμάτια συνομιλιών με οποιοδήποτε λογασιασμό [matrix] από matrix.org, αναζητώντας στο διακομιστή hellug.gr.

##### Περισσότερες υπηρεσίες μπορείτε να δείτε στο http://members.hellug.gr/

Η [Τεκμηρίωση για τις υπηρεσίες](https://wiki.hellug.gr/index.php?title=%CE%9A%CE%B1%CF%84%CE%B7%CE%B3%CE%BF%CF%81%CE%AF%CE%B1:%CE%A5%CF%80%CE%B7%CF%81%CE%B5%CF%83%CE%AF%CE%B5%CF%82) βρίσκεται στο [Hellug Wiki](https://wiki.hellug.gr)

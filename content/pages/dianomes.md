---
title: "Διανομές"
layout: "pages"
bannertemplate: "hellug-banner.html"
date: 2012-05-13
category_title: Linux
tags: [Διανομές]
linux: [dianomes]
menu:
    main:
        parent: "linux"
        weight: 4

---
        
Για νέους χρήστες:

Fedora ([fedoraproject.org](https://fedoraproject.org/))

Ubuntu ([ubuntu.com](http://ubuntu.com/))

MXLinux ([mxlinux.org](https://mxlinux.org/))

OpenSuse ([opensuse.org](http://opensuse.org/))

Για προχωρημένους χρήστες:

Arch ([archlinux.org](http://archlinux.org/))

Debian ([debian.org](http://debian.org/))

Gentoo ([gentoo.org](http://gentoo.org/))

Slackware ([slackware.com](http://slackware.com/))

Ελληνικές:

Antix ([antixlinux.org](http://antixlinux.org/))

Slackel ([slackel.gr](http://slackel.gr/))

Live:

Damn Small Linux ([damnsmalllinux.org](http://damnsmalllinux.org/))

Knoppix ([knoppix.org](http://knoppix.org/))

Puppy ([puppylinux.org](http://puppylinux.org/))

Slax ([slax.com](http://slax.com/))

Εξειδικευμένες:

Dynebolic ([dynebolic.org](http://dynebolic.org/))

Edubuntu ([edubuntu.org](http://edubuntu.org/))

Scientific ([scientificlinux.org](http://scientificlinux.org/))


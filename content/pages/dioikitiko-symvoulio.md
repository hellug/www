---
title: "Διοικητικό Συμβούλιο"
layout: "pages"
bannertemplate: "hellug-banner.html"
date: 2013-04-24
category_title: Σύλλογος
tags: [Διοικητικό Συμβούλιο]
peri-hellug: [dioikitiko-symvoulio]
menu:
    main:
        parent: "peri-hellug"
        weight: 3
---
        
Το ΔΣ παίρνει αποφάσεις σχετικές με το Σύλλογο.

Τα μέλη ΔΣ περιόδου 2024-25 είναι οι:
* Πρόεδρος - Ιάκωβος Στέλλας
* Αντιπρόεδρος - Richard Kweskin
* Γραμματέας - Γιάννης Τσιάγκας
* Ταμίας - Φάνης Δοκιανάκης
* Έφορος - Παναγιώτης Πάνος

--- 
Μπορείτε να επικοινωνίσετε με το ΔΣ, αποστέλοντας email στο board@hellug.gr

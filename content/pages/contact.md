---
title: "Επικοινωνία"
layout: "pages"
bannertemplate: "hellug-banner.html"
date: 2012-08-24
category_title: Επαφή
tags: [Επικοινωνία]
peri-hellug: [epikoinonia-1]
menu:
    main:
        parent: "contact"
        weight: 5
---

Forum
-----

Το forum του συλλόγου: [http://forum.hellug.gr/](http://forum.hellug.gr/)

MATRIX
------

Τα μέλη του hellug μπορούν να χρησιμοποιούν τον homeserver του συλλόγου "https://hellug.gr" και το web client στο https://im.hellug.gr. Τα matrix rooms είναι δημόσια και federated και μπορείτε να τα εξερευνήσετε και να συμμετέχετε από την εύρεση του matrix από οποιοδήποτε λογαριασμό σε άλλο homeserver (πχ matrix.org), [βάζοντας σα διακομιστή το hellug.gr](https://matrix.to/#/#hellug:hellug.gr).

IRC
---

IRC κανάλι #hellug στο libera.chat (μέσω [web browser](https://web.libera.chat/#hellug) μέσω [irc client](irc://irc.libera.chat/#hellug))

Mailing lists
-------------

Οι δημόσιες συζητήσεις για τα θέματα του σύλλογου Hellug γίνονται στη λίστα [Hellug](http://lists.hellug.gr/hellug.php), στην οποία μπορεί να [εγγραφεί](http://lists.hellug.gr/mailman/listinfo/hellug) οποιοσδήποτε ανεξάρτητα από το αν είναι μέλος του συλλόγου ή όχι. Η λίστα είναι διαθέσιμη και μέσω του local.linux.greek.hellug Usenet Newsgroup από τους εξυπηρετητές news.grnet.gr και news.ntua.gr.

H [Members](http://lists.hellug.gr/members.php) είναι η λίστα για τα εσωτερικά θέματα του συλλόγου (ανακοινώσεις, συζητήσεις). Απευθύνεται αποκλειστικά και μόνο στα μέλη του συλλόγου.

Άμεση Επικοινωνία
-----------------

Για να επικοινωνήσετε μαζί μας, στείλτε μας e-mail στο: board \[at\] hellug \[dot\] gr.

Κοινωνικά Δίκτυα
----------------

*   [Twitter](http://twitter.com/hellug "Follow HEL.L.U.G on Twitter")
*   [Facebook](https://www.facebook.com/groups/6210288060/ "Follow HEL.L.U.G on Facebook")
*   [LinkedIn](http://www.linkedin.com/groups/HELLUG-108292/about "Follow HEL.L.U.G on LinkedIn")
*   [Google+](https://plus.google.com/104927942089086340624/posts "Follow HEL.L.U.G on Google+")

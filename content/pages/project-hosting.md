---
title: "Project Hosting"
layout: "pages"
bannertemplate: "hellug-banner.html"
date: 2013-04-29
category_title: "Δράσεις"
tags: [Δράσεις]
menu:
    main:
        parent: "draseis"
        weight: 5
---
|Project|Link|
|---|---|
|Hellug Code Hosting|[dev.hellug.gr](https://dev.hellug.gr)
|OpenERP|[http://openerp.hellug.gr/](http://openerp.hellug.gr/)|
|~~Mcrypt~~|~~[http://mcrypt.hellug.gr/](http://mcrypt.hellug.gr/)~~|
|OpenOffice|[http://openoffice.hellug.gr/](http://openoffice.hellug.gr/)|
|Clonezilla Sysrecue|[http://clonezilla-sysresccd.hellug.gr/](http://clonezilla-sysresccd.hellug.gr/)|
|Imapfilter|[http://imapfilter.hellug.gr/](http://imapfilter.hellug.gr/)|
|XISP|[http://xisp.hellug.gr/](http://xisp.hellug.gr/)|
|Hydra Webserver|[http://hydra.hellug.gr/](http://hydra.hellug.gr/)|
|OpenEdu|[http://openedu.hellug.gr](http://openedu.hellug.gr/)|
|Argeas|[http://argeas.hellug.gr](http://argeas.hellug.gr/)|
|OLD git|[http://git.hellug.gr](http://git.hellug.gr/)|
|OLD subversion|[http://svn.hellug.gr](http://svn.hellug.gr/)|
|OLD cvs|[http://cvs.hellug.gr](http://cvs.hellug.gr/)|

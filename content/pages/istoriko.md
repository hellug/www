---
title: "Ιστορικό"
layout: "pages"
bannertemplate: "hellug-banner.html"
date: 2019-09-20
peri-hellug: [page]
tags: [peri-hellug]
menu:
  main:
      parent: "peri-hellug"
      weight: 2
---
## H πορεια του Συλλογου απο την ιδρυση του μέχρι σήμερα.

### Προϊδεασμός…..

Η σκέψη για την ίδρυση Συλλόγου µε θέμα το Linux (που τότε και πάντα, νοηματικά, εξέφραζε τον κώδικα-πυρήνα ενός νέου λειτουργικού συστήματος – Oparating System (O.S.), για “συμβατούς IBM Η/Υ”), προϋπήρχε σε μερικά νέα παιδιά στην Ελλάδα, τα έτη 1995-1997, ενώ παράλληλα ορισμένοι μεμονωμένοι χρήστες του νέου αυτού O.S., είχαν ήδη ιστοσελίδες για το θέμα αυτό.

Το 1996, ο Κώστας Βλάσσης δημιουργεί την ιστοσελίδα [www.compulink.gr/linux/](https://web.archive.org/web/19980117063131/www.compulink.gr/linux/) µε τίτλο Hellug. Η ιστοσελίδα εκείνη περιείχε συμβουλές, άρθρα αλλά και μια “πρώτη” καταμέτρηση των χρηστών Linux.

To 1997 δημιουργείται από τον Ι. Ιωάννου, η πρώτη mailing list για Linux linux-greek-users@argeas.argos.hol.gr, (που το 1998 “[μετακόμισε](http://lists.hellug.gr/lgu.php)” στον server TUX του ιδρυμένου Συλλόγου). Αντικείμενό της ήταν η επίλυση προβλημάτων στη χρήση του Linux αλλά και γενικότερα σε θέματα που αφορούν αυτό το λειτουργικό σύστημα.

Τον Δεκέμβριο 1997, οι Μιχάλης Καµπριάνης, Παναγιώτης Βρυώνης, Φώτης Γεωργάτος, Γιώργος Κεραμίδας και Γιώργος Κουλογιάννης, στήνουν το [www.linux.gr](https://www.linux.gr), τον πρώτο ελληνικό δικτυακό τόπο για το Linux, nou αποτέλεσε έκτοτε (και αποτελεί), σημείο αναφοράς για τους Έλληνες φίλους και χρήστες του Linux.

### Προς την ίδρυση…… To σωτήριο έτος 1998!

Tov Ιανουάριο 1998 κυκλοφορεί το διαδικτυακό περιοδικό [Magaz](http://magaz.hellug.gr/), ως καθαρά ηλεκτρονικό περιοδικό για τους φίλους και χρήστες του Linux στην Ελλάδα, με θέματα Linux και Ελεύθερου Λογισμικού γενικότερα. Στο ηλεκτρονικό αυτό περιοδικό έγραφαν [διάφοροι φίλοι και χρήστες του Λίνουξ](https://magaz.hellug.gr/authors.html) και αισίως έφτασε το 2006 στο [τεύχος 35](https://magaz.hellug.gr/35/), όπου και σταμάτησε η έκδοσή του.

Τον Απρίλιο 1998, ο Ιωάννου στήνει και τη mailing λίστα [hellug@argeas.argos.hol.gr](http://lists.hellug.gr/hellug.php) που ασχολείται με τα διαδικαστικά της ίδρυσης του Συλλόγου. Επίσης, γίνεται µια αρχική συνάντηση για το θέμα αυτό, ενώ οι Ευριπίδης Παπακώστας και Βούλα Σανιδά συλλέγουν τα νομικά έγγραφα που απαιτούνται. O Κώστας Μαϊστρέλης αναλαμβάνει χρέη γραμματέα, καταγράφει τους ενδιαφερόμενους και εκτυπώνει τα νομικά έγγραφα. Στα τέλη του Απριλίου, η Β.Σανιδά και ο Ε. Παπακώστας, ασχολούνται με το προσχέδιο του καταστατικού του μελλοντικού Συλλόγου. Ο Γιώργος Κεραμίδας αναλαμβάνει Ταμίας, ενώ ο Φώτης Γεωργάτος συλλέγει υπογραφές από ενδιαφερόμενους εκτός Αθηνών. Μέχρι τον Σεπτέμβριο 1998, έχει ανατεθεί σε δικηγόρο η επικείμενη ίδρυση του Συλλόγου. Στις 9/9/1998, εκλέγεται προσωρινό Δ.Σ. με Πρόεδρο τον Μιχάλη Καμπριάνη, Αντιπρόεδρο τον Ευριπίδη Παπακώστα, Γραμματέα τον Παναγιώτη Βρυώνη, Ταμία τη Β. Σανιδά και Έφορο το Φώτη Γεωργάτο.

Ενώ στις 28 Σεπτεμβρίου 1998, κατατίθεται πλέον η αίτηση σύστασης του 1ου Πανελλήνιου Συλλόγου, με την επισημοποίηση της ήδη υπάρχουσας ονομασίας Hellenic Linux Users Group (HEL.L.U.G.) – Ένωση Χρηστών και Φίλων Linux Ελλάδας (Ε.Χ.Φ.Λ.Ε.) Ο Κ. Μαϊστρέλης αναλαμβάνει την προμήθεια όλων των “γραφειοκρατικών αξεσουάρ” (βιβλία, αποδείξεις, σφραγίδες κ.λπ.).
Ταυτόχρονα αρχίζει η εγγραφή των μελών, ενώ εθελοντές αναλαμβάνουν τη διαχείριση του Website, της βάσης δεδομένων, και των λιστών e-mail.

Λίγες μέρες πριν, ο Σύλλογος κάνει… [επιδρομή στη οδό Στουρνάρη](https://photos.hellug.gr/index.php?/category/4), μοιράζοντας δωρεάν Linux Cds. Επικρατεί το….. αδιαχώρητο από περαστικούς που ενδιαφέρονται για το άγνωστο τότε λειτουργικό σύστημα, ενώ η εκδήλωση καλύπτεται από την ET3!
![Στουρνάρα 1998](https://photos.hellug.gr/galleries/Hellug_Happening_Stournara_1998/Hap15.jpg)

Παράλληλα, γίνεται η πρώτη απόπειρα δημιουργίας ενός [Λεξικού Μετάφρασης Όρων Πληροφορικής](https://web.archive.org/web/19991201100305/http://server.hellug.gr/LUGistics/el/pub/LEXIS_main.php3).

Η γραφίστρια Άννα Βρυώνη αναλαμβάνει το καλλιτεχνικό µέρος των ιστοσελίδων του www.hellug.gr, του οποίου η επίσημη λειτουργία ξεκινά στις 29 Οκτωβρίου.
Στα τέλη Σεπτεμβρίου 1998 γίνεται η συναρμολόγηση του “tux”, του πρώτου server του HEL.L.U.G.,

### Tα επόμενα χρόνια 1999-2005
Κατά τα έτη 1999-2000, τα μέλη και οι φίλοι του Συλλόγου HEL.L.U.G. συναντώνται στην έδρα του Συλλόγου στην Αγ. Παρασκευή Αττικής. Οργανώνουν διάφορες εκδηλώσεις που εξυπηρετούν την διάδοση του νέου Λειτουργικού Συστήματος Linux, μέσω κυρίως μερικών από τις τότε υπάρχουσες διανομές του. Επιπλέον ο Σύλλογος, υποστηριζόμενος και από μέλη της Θεσσαλονίκης, συμμετέχει για πρώτη φορά στην Έκθεση Πληροφορικής Infosystem Θεσσαλονίκης, με απρόσμενη επιτυχία, αφού πολύς κόσμος γνωρίζει για πρώτη φορά το Linux.

Οι οργανωμένες συναντήσεις στο σταθερό χώρο-Έδρα του Συλλόγου διακόπηκαν, όταν ορισμένα ιδρυτικά μέλη θέλησαν να αποσυρθούν από τα δρώμενα του Συλλόγου. Εντούτοις, τα μέλη και οι φίλοι του Συλλόγου συνεχίζουν να συναντιόνται, συνεννοούμενοι μέσω των mailing lists, μια φορά την εβδομάδα (Σαββάτο απόγευμα), στην οδό Θεμιστοκλέους στο κέντρο των Αθηνών, στο πατάρι του [Ξενοδοχείου “Εξάρχεια”](https://www.exarchion.com/). Έτσι οργανώνονται διάφορες δράσεις όπως τον Μάρτιο του 2001, που πραγματοποιείται το πρώτο install-fest Linux στην Ελλάδα, μια σημαντική και πρωτοποριακή εκδήλωση διάδοσης του Linux στον απλό κόσμο, που έγινε στο στεγασμένο υπαίθριο χώρο του Τµ. Πληρ/κής του Πανεπιστηµίου Αθηνών. Εκεί τα μέλη του Συλλόγου εγκαθιστούν Linux στον υπολογιστή κάθε ενδιαφερόμενου. Το flash.gr “καλύπτει” την εκδήλωση. Το επόμενο χρονικό διάστημα, ακολουθούν αντίστοιχα install-fest στο Τ.Ε.Ι. Πειραιά, στο Τ.Ε.Ι. Κρήτης κ.α

Το θέμα της έδρας του Συλλόγου εμφανίζεται και πάλι και αποτελεί ένα διοικητικό πρόβλημα για τα τρέχοντα Δ.Σ. του. Μια πενταμελής ομάδα μελών, αναλαμβάνει προσωρινό διοικητικό έργο, για να λύσει όπως μπορούσε το θέμα αυτό. Σε σχετική με το θέμα αυτό εκδήλωση-κοπή της πίττας του Συλλόγου τον Ιανουάριο 2002, σε αίθουσα του [Ξενοδοχείου “Ηνίοχος”](https://t-ec.bstatic.com/images/hotel/max200/478/4783406.jpg), παρουσιάζεται από το ΔΣ το πρόβλημα και αναζητούνται λύσεις με ανταλλαγή συζητήσεων με τα μέλη. Στην εκδήλωση αυτή παρίσταται μεταξύ των μελών και ένα νέο μέλος του Συλλόγου, ο R.Kweskin, η παρουσία του οποίου έπαιξε καθοριστικό ρόλο τα επόμενα χρόνια στο Σύλλογο.

----
Ένα μεγάλο κεφάλαιο για το σύλλογο έκλεισε με τη χθεσινή (25/9/2010) μετακόμιση του εξοπλισμού από το διαμέρισμα της Καλλιθέας όπου φιλοξενήθηκε για χρόνια το εργαστήριο του Hellug. Το μεγαλύτερο μέρος του υλικού κρίθηκε αρκετά (ως υπερβολικά) παλιό κι έτσι πήγε προς ανακύκλωση, ήταν κυρίως παλιές οθόνες, παλιοί ηλεκτρονικοί υπολογιστές και λοιπά εξαρτήματα όπως τροφοδοτικά, μητρικές κάρτες και σκληροί δίσκοι. Όσα υλικά κρίθηκε πως θα μπορούσαν να είναι χρήσιμα για το μέλλον κρατήθηκαν.

Ο εξοπλισμός ο οποίος [μεταφέρθηκε](https://photos.hellug.gr/index.php?/category/3) προς (προσωρινή) αποθήκευση στο γραφείο του ταμία μας Πάνου Χρηστέα μπορεί να αποτελέσει τη βάση για κάποιο μελλοντικό εργαστήριο αν υπάρχει αντίστοιχη πρωτοβουλία κάποια στιγμή από τα μέλη μας. Επιπλέον είναι πράγματα που θα μπορούσαν να χρησιμοποιηθούν σε κάποια έκθεση ή άλλη εκδήλωση (πχ τραπέζια και καρέκλες).

Από αυτήν την τελευταία επίσκεψη μελών του συλλόγου στο διαμέρισμα της Καλλιθέας υπάρχει και [φωτογραφικό υλικό](https://photos.hellug.gr/index.php?/category/31) ως ενθύμιο.

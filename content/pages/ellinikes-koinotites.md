---
title: "Ελληνικές Κοινότητες"
layout: "pages"
bannertemplate: "hellug-banner.html"
date: 2013-06-29
category_title: Linux
tags: [Κοινότητες]
linux: [koinotites]
menu:
    main:
        parent: "linux"
        weight: 4

---

Εκτός από το Hellug οι παρακάτω κοινότητες δραστηριοποιούνται στην Ελλάδα

| Κοινότητα | Πόλη/Τόπος | Link |Another-link|
|---|---|---|---|
|LUG|Αλεξανδρούπολη|allug|
|LUG|~~Βόλος~~|[~~volug~~](https://web.archive.org/web/20110903105734/https://volug.gr/)|
|LUG|~~Ηράκλειο~~|[~~herlug~~](https://lists.hellug.gr/mailman/listinfo/herlug/)|
|Σύλλογος|Θεσσαλονίκη|[Greeklug](https://www.greeklug.gr/)|
|LUG|~~Ιωάννινα~~|[~~ilug~~](https://web.archive.org/web/20190812190055/https://ilug.gr/forum)|
|Σύλλογος|Κοζάνη|[Klug](https://klug.gr/)|
|LUG|Κομοτηνή|thracelug|
|LUG|~~Μυτιλήνη~~|[~~mlug~~](https://groups.google.com/group/mytilene-linux-user-group)|
|LUG|~~Πάτρα~~||[~~plug~~](https://web.archive.org/web/20150716154121/https://www.patras-lug.gr/)|
|LUG|Πτολεμαϊδα|[plug](https://www.plug.gr/)|[forum.plug.gr](https://forum.plug.gr/)|
|LUG|~~Σέρρες~~|[~~serlug~~](https://teiserron.gr/)|
|LUG|~~Σύρος~~|[~~sylug~~](https://web.archive.org/web/20120113160631/https://cywn.dyndns.org/)|
|Σύλλογος|~~Χανιά~~|[~~chania-lug~~](https://web.archive.org/web/20190113123512/https://www.chania-lug.gr/)|
|Σύλλογος|~~Ζάκυνθος~~|[~~zantelug~~](https://web.archive.org/web/20110816173545/https://www.zantelug.gr/zlug/)|
|LUG|Κύπρος|[EllakCy](https://ellak.org.cy/)|
|LUG|~~Κύπρος~~|[~~Cyprus LUG~~](https://web.archive.org/web/20130218112058/https://www.cypruslug.org/)|
|ΑΕΙ/foss|~~Οικονομικό Πανεπιστήμιο Αθηνών~~|[~~foss.aueb.gr~~](https://web.archive.org/web/20211128142631/https://foss.aueb.gr/)|
|ΑΕΙ/foss|~~Πανεπιστήμιο Αιγαίου~~|[~~foss.math.aegean.gr~~](https://web.archive.org/web/20120115091748/https://foss.math.aegean.gr/)|
|AΕΙ/foss|~~Πολυτεχνείο Αθηνών~~|[~~foss.ntua.gr~~](https://web.archive.org/web/20161019042909/https://foss.ntua.gr/wiki/index.php/%CE%91%CF%81%CF%87%CE%B9%CE%BA%CE%AE_%CF%83%CE%B5%CE%BB%CE%AF%CE%B4%CE%B1)|
|ΑΕΙ/foss|Πανεπιστήμιο Μακεδονίας|[opensource uom](https://opensource.uom.gr)|
|AΕΙ/foss|~~Πανεπιστήμιο Πειραιά~~|[~~Κοινότητα Ανάπτυξης Ελεύθερου Λογισμικού~~](https://web.archive.org/web/20171125001757/https://rainbow.cs.unipi.gr/projects/oss/)|
|ΑΕΙ/foss|~~Πολυτεχνείο Κρήτης~~|[~~ellak tuc~~](https://web.archive.org/web/20120221004543/https://ellak.tuc.gr/)|
 |ΑΕΙ/foss|~~ΤΕΙ Θεσ/νίκης~~|[~~teithe free~~]()|
|ΑΕΙ/foss|~~ΤΕΙ Λάρισας~~|[~~linuxteam teilar~~](https://web.archive.org/web/20110820030854/https://linuxteam.cs.teilar.gr/)|
|ΑΕΙ/foss|~~ΤΕΙ Ηρακλείου~~|[~~LUG ΤΕΙ Κρήτης~~](https://web.archive.org/web/20110820084011/https://lug.teicrete.gr/)|
|Διανομή|Debian Gr|[Debian GR](https://web.archive.org/web/20120428134658/https://www.debian.gr/)|
|Διανομή|Ubuntu GR|[Ubuntu](https://ubuntu-gr.org/)|ubuntu-gr.org/ [ubuntu-gr](https://lists.ubuntu.com/mailman/listinfo/ubuntu-gr) [wiki.ubuntu-gr.org/Wiki](https://wiki.ubuntu-gr.org/Wiki)|
|Διανομή||[slackel](https://slackel.gr/)|
|Διανομή||[antix](https://antix.mepis.org/)|
|Διανομή|~~Archlinux GR~~|[archlinuxgr](https://lists.hellug.gr/mailman/listinfo/archlinuxgr)|
|Project|~~PostgreSQL Greece~~|[~~postgresql.gr~~](https://web.archive.org/web/20190823012551/https://postgresql.gr/)|
|Project|Java User Group|[www.jhug.gr/](https://www.jhug.gr/)|
|Project|Joomla User Group|[www.joomla.gr/](https://www.joomla.gr/)|
|Project|Wordpress|[wpgreece](https://www.wpgreece.org/)|
|Project|~~OWASP GR~~|[~~owasp.wordpress.com~~](https://owasp.wordpress.com/)|
|Hackerspace|Αθήνα|[HackerSpace.gr](https://hackerspace.gr/)|
|Hackerspace|~~Θεσσαλονίκη~~|[~~Thessaloniki’s Hackerspace~~](https://web.archive.org/web/20120801081359/https://www.the-hackerspace.org/)|
|Hackerspace|Ηράκλειο-Κρήτης|[Το labaki](https://wiki.tolabaki.gr/w/To_LABaki)|
|Hackerspace|~~Πάτρα~~|[~~p-space~~](https://web.archive.org/web/20170606032254/https://www.p-space.gr/)|

---

### Online Κοινότητες

fora:
*   [Hellug Forum](https://forum.hellug.gr/)
*   [linux-user.gr](https://linux-user.gr/)
*   [forum.ubuntu-gr.org](https://forum.ubuntu-gr.org/)
*   [adslgr.com unix-linux](https://www.adslgr.com/forum/forumdisplay.php?f=42)
*   [insomnia.gr linux](https://www.insomnia.gr/forums/forum/38-linux/)

λίστες ταχυδρομείου:
*   [linux-greek-users](https://lists.hellug.gr/mailman/listinfo/linux-greek-users)
*   [migrate2linux](https://lists.hellug.gr/mailman/listinfo/migrate2linux)
*   [public hellug](https://lists.hellug.gr/mailman/listinfo/public)

wikis:
*   [wiki.hellug.gr](https://wiki.hellug.gr)
*   [lgu-faq.hellug.gr](https://lgu-faq.hellug.gr/)
*   [openoffice.hellug.gr](https://openoffice.hellug.gr/)

portals:
*   [ellak.gr](https://ellak.gr/)
*   [epatents.hellug.gr](https://epatents.hellug.gr/)
*   [linux.gr](https://linux.gr/)
*   [planet.ellak.gr](https://planet.ellak.gr/)

τεκμηρίωση:
*   [howto.hellug.gr](https://howto.hellug.gr/)
*   [magaz.hellug.gr](https://howto.hellug.gr/)
*   [Ασύρματο Μητροπολιτικό Δίκτυο Αθήνας](https://www.awmn.net/)
*   [~~Ασύρματο Δίκτυο Θεσσαλονίκης~~](https://web.archive.org/web/20080730160414/https://www.twmn.net/)
*   [Ασύρματες Κοινότητας Ελλάδας](https://www.awmn.net/content.php?r=168)
*   [Σύλλογος Ραδιοερασιτέχνων Ελλάδας](https://www.grc.gr/index.html)

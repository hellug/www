---
title: "Τεχνική Υποστήριξη"
layout: "pages"
bannertemplate: "hellug-banner.html"
date: 2022-05-10
category_title: Σύλλογος
tags: [Τεχνική Υποστήριξη]
peri-hellug: [techiki-ypostirixi-1]
menu:
    main:
        parent: "peri-hellug"
        weight: 6
---

Ο σύλλογος προσθέρει εθελοντική τεχνική υποστήριξη σε ότι αφορά τα λειτουργικά συστήματα βασισμένα στο Linux και παρεμφερείς τεχνολογίες ελεύθερου λογισμικού ή υλικού.

Τα παρακάτω κανάλια επικοινωνίας είναι διαθέσιμα για όλους.

Meetups
---

Με φυσική παρουσία στα προγραμματισμένα Meetups. (Προσωρινα αναβλήθηκαν λόγω πανδημίας)

Forum
-----

Το forum του συλλόγου: [http://forum.hellug.gr/](http://forum.hellug.gr/) στις κατηγορίες "Τεχνικά Θέματα" και "Νέοι Χρήστες".

Matrix
------

Στην ομόσπονδη υπηρεσία Matrix [Δωμάτιο Tech-Support](https://matrix.to/#/#tech-support:hellug.gr).

Mailing lists
-------------

Στη λίστα αλληλογραφίας [Linux-Greek-Users](http://lists.hellug.gr). Η λίστα είναι διαθέσιμη και μέσω του local.linux.greek.users Usenet Newsgroup από τους εξυπηρετητές news.grnet.gr και news.ntua.gr.

---
title: "Δήλωση Ιδιωτικότητας"
layout: "pages"
bannertemplate: "hellug-banner.html"
date: 2022-05-10
category_title: Σύλλογος
tags: [Ιδιοτικότητα]
peri-hellug: [idiotikotita-1]
menu:
    main:
        parent: "peri-hellug"
        weight: 7
---

Cookies
---

Οι υπηρεσίες του Hellug χρησιμοποιούν Cookies μόνο για λόγους λειτουργικότητας και διαλειτουργικότητας μεταξύ των υπηρεσιών. Δεν χρησιμοποιούνται cookies τρίτων.

Συλλογή Στατιστικών Χρήσης
---

Χρησιμοποιείται αυστηρά μόνο μία υπηρεσία για τη συλλογή στατιστικών χρήσης για τη βελτιώση των υπηρεσιών μας, και αυτή είναι εσωτερική και διαχειρίζεται από το Σύλλογο.
Μπορείτε να κάνετε opt-out από αυτή τη συλλογή στατιστικών από το [σύνδεσμο αυτό](https://stats.hellug.gr/index.php?module=CoreAdminHome&action=optOut&language=en&backgroundColor=&fontColor=&fontSize=&fontFamily=%22) ή θέτοντας την επιλογή DoNotTrack στο πρόγραμμα περιήγησης σας. Πχ [Chrome](https://support.google.com/chrome/answer/2790761?hl=en&co=GENIE.Platform%3DDesktop) ή
[Firefox](https://support.mozilla.org/en-US/kb/how-do-i-turn-do-not-track-feature)

Δε χρησιμοποιείται καμμία τεχνολογία καταγραφής δραστηριοτήτων του χρήστη και τα δεδομένα δεν αποστέλονται σε εξωτερικές υπηρεσίες.

Δικαιώματα ΓΚΠΔΠΧ (GDPR)
---

Για να εξασκίσετε τα δίκαιόματα σας σχετικά με το Γενικό Κανονισμό Προστασίας Δεδομένων Προσωπικού Χαρακτήρα (GDPR), παρακαλώ στείλτε το αίτημα σας μέσω ηλεκτρονικής αλληλογραφίας (e-mail) στη διεύθυνση board@hellug.gr.

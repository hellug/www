---
title: "Credits"
bannertemplate: "null.html"
build:
  list: never
---

    
### Συντελεστές

##### Η ιστοσελίδα αυτή έχει γραφτεί από τους:

* Ιάκωβο Στέλλα
* Πάνο Χρηστέα
* Φάνη Δοκιανάκη
* Γιώργο Δημητρακόπουλο

##### Ελεύθερο Λογισμικό που χρησιμοποιούμε
* Γεννήτρια Στατικών Ιστοσελίδων [Hugo](https://gohugo.io)
* Βιβλιοθήκη CSS [Bootstrap](https://getbootstrap.com)
* Ασύγχρονες κλήσεις Rest μέσω [HTMX](https://htmx.org)
* Περιβάλλον συστήματος βασισμένο σε [Go](https://golang.org)
* Δρομολογητής HTTP [echo](https://echo.labstack.com)
* Εξυπηρετητής κάψουλας Gemini [Agate](https://github.com/mbrubeck/agate)
* Συνεργατικό περιβάλλον ανάπτυξης [Forgejo](https://forgejo.org/)
* Κατανεμημένο σύστημα ελέγχου εκδόσεων [Git](https://git-scm.com)

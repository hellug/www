---
title: "Πρόοδος για την λειτουργία της Ειδικότητας ΔΙΕΚ *Τεχ. Συστημάτων Ανοικτού Λογισμικού*"
layout: "news"
bannertemplate: "hellug-banner.html"
date: 2022-09-12
category_title: Δράσεις
tags: [Νέα]
draseis: [news]
summary: "Η νέο-ιδρυμένη Πειραματική Ειδικότητα ΔΙΕΚ **Τεχν. Συστημάτων Ανοικτού Λογισμικού**, προετοιμάζεται να λειτουργήσει στο Θεματικό ΔΙΕΚ Αιγάλεω, από τον Φεβρουάριο 2023...."
---
Με παρελθούσα [Ανακοίνωσή](https://www.hellug.gr/p/eidikotita-anoiktou-logismikou/) μας (πριν περίπου ένα χρόνο), είχαμε γνωστοποιήσει την Ίδρυση μιας νέας Ειδικότητας στο χώρο της Αρχικής Επαγγελματικής Εκπαίδευσης και Κατάρτισης για την έγκριση της οποίας είχε συνεισφέρει και ο Σύλλογος. 

![screenshot](/media/DIEK-FOSS/odigos-neas-eidikothtas.png)

Το διάστημα που πέρασε, η Ειδικότητα αυτή απέκτησε [**Επαγγελματικό Περίγραμμα και Οδηγό Σπουδών**](https://iekaigal.att.sch.gr/?p=2198) και πλέον, με βάση τον Οδηγό, ξεκινά και η συγγραφή του Εκπαιδευτικού Υλικού. Όπως μπορείτε να δείτε, σύμφωνα με το ΔΙΕΚ Αιγάλεω, η Ειδικότητα αναμένεται να εγκριθεί από τη Γ.Γ.Ε.Ε.Κ.Δ.Β.Μ.&Ν. για να ξεκινήσει την λειτουργία της από την επόμενη χρονιά, αν και γίνονται προσπάθειες να ξεκινήσει τον ερχόμενο Φεβρουάριο 2023.

Ελπίζουμε να ξεκινήσει όντως σύντομα η νέα Ειδικότητα, ώστε να προκύψουν με το πέρασμα του χρόνου, και οι πρώτοι Απόφοιτοί της ! 





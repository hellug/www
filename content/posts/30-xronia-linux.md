---
title: "30 Χρόνια Linux"
layout: "news"
bannertemplate: "hellug-banner.html"
date: 2021-08-25
category_title: Δράσεις
tags: [Νέα]
draseis: [news]
summary: "Πριν 30 χρόνια σαν σήμερα στις 25 Αυγούστου 1991 ο τότε φοιτητής στο πολυτεχνείο του Ελσίνσκι, Linux Torvalds θα ανακοινώσει την ανάπτυξη ενός λειτουργικού συστήματος που δεν θα ήταν μεγάλο και επαγγελματικό όπως το GNU (HURD)."
---

Πριν 30 χρόνια σαν σήμερα στις 25 Αυγούστου 1991 ο τότε φοιτητής στο πολυτεχνείο του Ελσίνσκι, Linux Torvalds θα ανακοινώσει την ανάπτυξη ενός λειτουργικού συστήματος που δεν θα ήταν μεγάλο και επαγγελματικό όπως το GNU (HURD).

To μήνυμα στην ομάδα νέων (newsgroup) usenet comp.os.minix :

>    From:torvalds@klaava.Helsinki.FI (Linus Benedict Torvalds)  
>    Newsgroup: comp.os.minix  
>    Subject: What would you like to see most in minix?  
>    Summary: small poll for my new operating system  
>    Message-ID: 1991Aug25, 20578.9541@klaava.Helsinki.FI  
>    Date: 25 Aug 91 20:57:08 GMT  
>    Organization: University of Helsinki.  
>
>    Hello everybody out there using minix-
>
>    I’m doing a (free) operating system (just a hobby, won’t be big
>    and professional like gnu) for 386(486) AT clones. This has
>    been brewing since april, and is starting to get ready. I’d like
>    any feedback on things people like/dislike in minix; as my OS
>    resembles it somewhat (same physical layout of the file-sytem
>    due to practical reasons)among other things.
>
>    I’ve currently ported bash (1.08) an gcc (1.40), and things seem to work.
>    This implies that i’ll get something practical within a few months, and I’d
>    like to know what features most people want. Any suggestions are welcome,
>    but I won’t promise I’ll implement them :-)
>
>    Linus Torvalds

Χρόνια πολλά Linux

---

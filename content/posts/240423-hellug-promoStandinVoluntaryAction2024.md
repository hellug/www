---
title: "HEL.L.U.G. Posters in Voluntary Action 2024"
layout: "news"
bannertemplate: "hellug-banner.html"
date: 2024-04-15
category_title: Δράσεις
tags: [Νέα]
draseis: [news]
summary: "Οι Αφίσσες του HEL.L.U.G. στο Φεστιβάλ Εθελοντισμού Voluntary Action 2024!"
---
Το [**Φεστιβάλ Εθελοντισμού Voluntary Action 2024**](https://www.voluntaryaction.gr/) **πλησιάζει..!!!**
Ο **Σύλλογος HEL.L.U.G.**, [**θα είναι εκεί**](https://www.hellug.gr/p/meetup-240423-hellug-standinvoluntaryaction2024/) και τις 3 ημέρες λειτουργίας του Φεστιβάλ, με **Περίπτερο/Stand!**.

Ο [Διοργανωτής του Φεστιβάλ](https://www.skywalker.gr/el), μας διαθέτει τις επόμενες 2 Αφίσσες, που αφορούν την συμμετοχή μας και βέβαια μπορούν να αναδιανεμηθούν στα Κοινωνικά Δίκτυα.

| <u>Αφίσσα 1</u> | <u>Αφίσσα 2</u> |
| :-----------: | :-----------: |
| {{< figure src="/media/Hellug-StandinVoluntaryAction2024/hellug-VoluntaryAction2024_Postcopy42.jpg" width="400" alt="Poster1" class="text-center" >}}| {{< figure src="/media/Hellug-StandinVoluntaryAction2024/hellug-VoluntaryAction2024_Storycopy42.jpg" width="400" alt="Poster2" class="text-center" >}} |

Το Φεστιβάλ αναμένεται:
- να το επισκευθούν Σχολεία και Ομάδες μαθητών, σπουδαστών και φοιτητών
- να το επισκευθούν αρκετοί/-ες, που ενδιαφέρονται για το θέμα
- να είναι μια ευκαιρία "οριζόντιας" Δικτύωσης με διάφορους Φορείς


#### Ως εκ τούτου, υπενθυμίζουμε ότι το Περίπτερο/Stand μας, θα χρειαστεί ΔιαΖώσης Υποστήριξη !

Προτρέπονται τα **Μέλη και οι Φίλοι του Συλλόγου,** να εκδηλώσουν την διαθέσιμότητά τους [**στη σχετική Φόρμα Google**](https://forms.gle/YnaN3LtDDtjZi3mT6), που δημιουργήθηκε για το σκοπό αυτό.

**Σας περιμένουμε ΟΛΟΥΣ....!**

ΔΣ HEL.L.U.G.


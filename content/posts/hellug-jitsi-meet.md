---
title: "Νέα Υπηρεσία Τηλεδιασκέψεων από το HELLUG"
layout: "news"
bannertemplate: "hellug-banner.html"
date: 2021-11-19
category_title: Δράσεις
tags: [Νέα]
draseis: [news]
---
Νέα υπηρεσία προς τα μέλη του συλλόγου, για τη φιλοξενία συνδιασκέψεων μέσω φωνής και video βασισμένη στο δημοφιλές έργο ελεύθερου λογισμικού [jitsi meet](https://jitsi.org).

Η υπηρεσία φιλοξενείται στο [meet.hellug.gr](https://meet.hellug.gr) και η δημιουργία δωματίου συνδιασκέψεων απαιτεί λογαριασμό βασισμένο σε διεύθυνση e-mail hellug.gr ή linux.gr. Τα μέλη μπορούν να δημιουργούν λογαριασμό στο σύστημα [hellug sso](https://hellug.gr/authsrv/realms/hellug/account/).

Στη συνέχεια οι υπόλοιποι συμμετέχοντες μπορούν να συνδέονται στο δωμάτιο ανώνυμα.

Με εκτίμηση,
Διαχειριστές Συστημάτων Hellug

---

---
title: "Παρουσία του HELLUG στο μικρό διαδίκτυο"
layout: "news"
bannertemplate: "hellug-banner.html"
date: 2021-10-06
category_title: Δράσεις
tags: [Νέα]
draseis: [news]
---
Ενεργοποιήθηκε η διαδικτυακή παρουσία του HELLUG στο «μικρό διαδίκτυο» με την κάψουλα gemini στο [gemini://hellug.gr](gemini://hellug.gr) . Το gemini είναι ένα ανοικτό προτόκολλο το οποίο αναπτύχθηκε πρόσφατα και είναι μια προσπάθεια για ένα πιο «ελαφρύ» και απλοποιημένο web, πλησιάζοντας το κλασσικό gopher.

Λεπτομέρειες
* [Gemini Protocol](https://gemini.circumlunar.space/docs/)
* [Λογισμικό πρόσβασης στο Gemini](https://gemini.circumlunar.space/software/)


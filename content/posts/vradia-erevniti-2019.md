---
title: "Ο HELLUG στη Βραδιά Ερευνητή 2019"
layout: "news"
bannertemplate: "hellug-banner.html"
date: 2019-09-26
eventdate: 2019-09-27
category_title: Δράσεις
tags: [Εκδηλώσεις, Νέα]
draseis: [news, event]
summary: "Τη Παρασκευή 27 Σεπτεμβρίου 2019, ο Σύλλογος θα έχει παρουσία στην Βραδιά Ερευνητή 2019 που θα γίνει στο Εθνικό Μετσόβιο Πολυτεχνείο, Κτίριο Αβέρωφ, Ιστορικό Συγκρότημα Πατησίων."
---
![](https://www.hellug.gr/media/researchersnight2019.png)

Προσκαλούμε όλα τα μέλη και τους φίλους του Συλλόγου στη Βραδιά Ερευνητή 2019, που θα γίνει τη τελευταία Παρασκευή του Σεπτεμβρίου και ώρα 17:00 στο Εθνικό Μετσόβιο Πολυτεχνείο, Κτίριο Αβέρωφ, Ιστορικό Συγκρότημα Πατησίων.

Ο Σύλλογος θα συμμετέχει σαν εκθέτης με θέμα τη γνωριμία, εκμάθηση χρήσης του Linux και του Ελεύθερου Λογισμικού/Λογισμικού Ανοικτού Κώδικα – ΕΛ/ΛΑΚ μεταξύ των χρηστών ΗΥ, διάδοση της αξίας του ΕΛ/ΛΑΚ, προσφορά εθελοντικής βοήθειας σε Σχολικούς Εργαστηριακούς Εκπαιδευτικούς χώρους Α'θμιας & Β'θμιας Εκπαίδευσης, γενικότερη προώθηση του Ελεύθερου Λογισμικού στην Εκπαίδευση και σε κάθε ενδιαφερόμενο.

Περισσότερα στην [επίσημη ιστοσελίδα της διοργάνωσης](http://www.researchersnight.gr/athina.el.aspx).

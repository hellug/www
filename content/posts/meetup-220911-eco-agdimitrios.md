---
title: "Επαφή HEL.L.U.G. με άλλες Συλλογικότητες"
layout: "meetups"
bannertemplate: "hellug-banner.html"
date: 2022-09-08
category_title: Δράσεις
tags: [Meetups]
draseis: [meetups]
meetupdate: 2022-09-11T18:00:00+03:00
meetuplocation: "Δημοτ. Λαχανόκηποι Αγ.Δημητρίου Αττικής/ Community Gardens"
summary: "Ο Σύλλογος HEL.L.U.G. θα έχει την ευκαιρία να μάθει περισσότερα για τις Ελεύθερες Δράσεις άλλων Ομάδων, επιδρώντας αλληλέγγυα με αυτές."
---

Μέλη & Φίλοι Hel.l.u.g.,

Ο Σύλλογος θα επισκεφθεί την **Κυριακή 11/09/2022 από τις 18:00 έως τις 21:00**, [την "πράσινη, αλληλέγγυα, οικολογική περιβαλλοντική γιορτή" για τα 10 χρόνια λειτουργίας](https://www.facebook.com/%CE%94%CE%B7%CE%BC%CE%BF%CF%84%CE%B9%CE%BA%CE%BF%CE%B9-%CE%9B%CE%B1%CF%87%CE%B1%CE%BD%CE%BF%CE%BA%CE%B7%CF%80%CE%BF%CE%B9-%CE%94%CE%B7%CE%BC%CE%BF%CF%85-%CE%91%CE%B3%CE%B9%CE%BF%CF%85-%CE%94%CE%B7%CE%BC%CE%B7%CF%84%CF%81%CE%B9%CE%BF%CF%85-Community-Gardens-Greece-214633485736305/) των [Δημοτικών Λαχανόκηπων Αγίου Δημητρίου Αττικής/ Community Gardens Greece](https://dad.gr/environment/perivallon/dimotikoi-lachanokipoi/).

Εκτός από την επαφή μας με ανθρώπους/ εκφραστές της εθελοντικής συλλογικότητας και αλληλεγγύης σε μια τοπική κοινωνία, θα έχουμε την ευκαιρία να γνωρίσουμε επιπλέον και το [Helios Eco Lab](https://www.dconform.com/helios-eco-lab), έναν από τους συμμετέχοντες Φορείς. Ο Φορέας αυτός, μεταξύ άλλων, εστιάζει την δράση του και στον τομέα της ενεργειακής αυτοτέλειας με ελεύθερη και αυτόνομη χρήσης της ενέργειας από φυσικές πηγές και στην "γιορτή", θα παρουσιάσει τον [τρόπο αξιοποίησης της ηλιακής ενέργειας για ... μαγείρεμα!!](https://kede.gr/prasini-giorti-gia-ta-10chrona-ton-dimotikon-lachanokipon-agiou-dimitriou/)

Σας περιμένουμε στο χώρο της γιορτής, [Εθνομαρτύρων 61 & Κουντουριώτου - Αγ.Δημήτριος - Αττικής](https://osm.org/go/xxH3pukSg-?layers=N&way=174930167), για να ξεναγηθούμε και να μάθουμε περισσότερα....!!

<p>Σχετικό φωτογραφικό υλικό, μπορείτε να βρείτε στο <a href="https://photos.hellug.gr/index.php?/category/38" target="_blank">20220911-Επαφή HEL.L.U.G. με άλλες Συλλογικότητες</a>


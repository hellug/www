---
title: "Νέα Υπηρεσία Συνομιλιών από το HELLUG"
layout: "news"
bannertemplate: "hellug-banner.html"
date: 2021-09-20
category_title: Δράσεις
tags: [Νέα]
draseis: [news]
---
Ενεργοποιήθηκε η νέα υπηρεσία διαδικτυακών συνομιλιών https://im.hellug.gr για τα μέλη και φίλους του συλλόγου. Βασίζεται στο ελεύθερο και ανοιχτό προτόκολλο [matrix] και είναι federated με άλλους διακομιστές matrix. Αν έχετε ήδη λογαριασμό [matrix] [αναζητήστε τα δωμάτια συνομιλιών](https://matrix.to/#/#hellug:hellug.gr) στο homeserver hellug.gr.

Υπενθυμίζουμε τη νέα μας υπηρεσία git hosting και συνεργατικής ανάπτυξης βασισμένη στο έργο ελεύθερου λογισμικού Gitea, στη τοποθεσία [dev.hellug.gr](https://dev.hellug.gr). Oι εγγραφές είναι ανοιχτές για μέλη και φίλους του συλλόγου με διευθύνσεις ταχυδρομείου @hellug.gr και @linux.gr. Όποιοι άλλοι θέλουν λογαριασμό, στείλτε e-mail στο dev@hellug.gr.

---

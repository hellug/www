---
title: "Fosscomm 2022: Ομιλία & Παρουσίαση Μέλους μας!"
layout: "news"
bannertemplate: "hellug-banner.html"
date: 2022-11-16
eventdate: 2022-11-19
category_title: Δράσεις
tags: [Εκδηλώσεις, Νέα]
draseis: [news, event]
summary: "**Το Ετήσιο Πανελλήνιο Συνέδριο Κοινοτήτων ΕΛ/ΛΑΚ Free and Open Source Software Communities Meeting-Fosscomm2022, αρχίζει...!!!**" 
---

Το Ετήσιο Πανελλήνιο Συνέδριο Κοινοτήτων ΕΛ/ΛΑΚ **[Free and Open Source Software Communities Meeting - Fosscomm 2022](https://2022.fosscomm.gr/)** θα πραγματοποιηθεί στην Λαμία **από 18–20 Νοεβρίου 2022 !** Προσκαλούνται όλα τα Μέλη και οι Φίλοι του Συλλόγου, να παρακολουθήσουν πολλές από τις ενδιαφέρουσες Ομιλίες/ Παρουσιάσεις αλλά και τα Εργαστήρια, ανάλογα με τα ενδιαφέροντά τους, που περιλαμβάνει το [Πρόγραμμα του Συνεδρίου](https://pretalx.fosscomm.gr/fosscomm2022/schedule/#) ..! 

Στο Πρόγραμμα, περιλαμβάνεται και η Ομιλία/ Παρουσίαση με τίτλο :
|**Μεταφράσεις στα Ελληνικά, έργων κώδικα του Raspberry Pi Foundation**,|
|:-:|
|***το Σαββάτο 19/11/2022, 13:00–13:30*** ,|


από την Ελληνική Εθελοντική Ομάδα μετάφρασης του [Raspberry Pi Foundation](https://projects.raspberrypi.org/en/projects/translating-for-raspberry-pi), στην οποία συνεισφέρει το [Μέλος μας Σ. Μισθού](https://pretalx.fosscomm.gr/fosscomm2022/speaker/C8YWLB/). Θα παρουσιαστούν τα **έργα κώδικα** που μεταφράζει η Ομάδα και απευθύνονται σε μαθητές και μαθήτριες 7-19 ετών, καθώς και σε ομάδες προγραμματισμού, όπως [CoderDojo](https://coderdojo.com/en/) και [Code Clubs](https://codeclub.org/en/). Επίσης θα παρουσιαστούν : 

1. ***η διαδικασία της μετάφρασης*** και 
2. ***o τρόπος συμμετοχής των Εθελοντών/-τριών***.

Μπορείτε να παρακολουθήσετε ***εξ αποστάσεως*** τις εργασίες του Fosscomm 2022, μέσα από τα κανάλια ["Ζωντανής Ροής"](https://2022.fosscomm.gr/%ce%b6%cf%89%ce%bd%cf%84%ce%b1%ce%bd%ce%ae-%cf%81%ce%bf%ce%ae/) του Συνεδρίου ! 



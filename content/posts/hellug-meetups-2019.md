---
title: "Hellug Meetups 2019"
layout: "meetups"
bannertemplate: "hellug-banner.html"
date: 2019-05-31
category_title: Δράσεις
tags: [Meetups]
draseis: [meetups]
meetupdate: 2019-07-14T20:30:00+03:00
meetuplocation: "MOZART CALDI"
summary: "Κάθε Κυριακή βρισκόμαστε για δουλειές, τεχνικές συζητήσεις, καφέ και ποτάκι."
---

**ΚΑΙ ΑΥΤΗ** .....την Κυριακή 14/07/19 στις 20.30, θα ξανα-βρεθούμε, στο MOZART CALDI Χρυσοστόμου Σμύρνης 14, Αιγάλεω!

Σχόλια!

1. είναι 5 στενά από την Εκκλησία του Εσταυρωμένου (σταθμός ΜΕΤΡΟ-ΑΙΓΑΛΕΩ), "καρφί" προς την Θηβών.
2. Η οδός Χρ. Σμύρνης είναι μικρός παράδρομος (πεζόδρομος) επί της Θηβών!

Δείτε στα:

[Χάρτης Vrisko](https://www.vrisko.gr/details/map/71_i275h102f6j613414_eci61ac06_a?LogEntryType=ShowMap)

[Google Map](https://www.google.gr/maps/uv?hl=el&pb=!1s0x14a1bca5502323d7:0x2258f6def6a0070b!2m22!2m2!1i80!2i80!3m1!2i20!16m16!1b1!2m2!1m1!1e1!2m2!1m1!1e3!2m2!1m1!1e5!2m2!1m1!1e4!2m2!1m1!1e6!3m1!7e115!4shttps://lh5.googleusercontent.com/p/AF1QipO41IyEGHfRFLduVcdNA4_FKJCCN0sMbsrb4tWf%3Dw260-h175-n-k-no!5zz4nPgc6xzrnOtc-CIM66zrHPhs61z4TOtc-BzrnOtc-CIM6xzrnOs86xzrvOtc-JIC0gzpHOvc6xzrbOrs-EzrfPg863IEdvb2dsZQ&imagekey=!1e10!2sAF1QipO41IyEGHfRFLduVcdNA4_FKJCCN0sMbsrb4tWf)

[Mozart Cald Facebook Page](https://el-gr.facebook.com/Mozart-caldi-cafe-restaurant-203116259707785/)

Ως συνήθως, μένουμε μέχρι τις 22.30 ή/και αργότερα...
....(ανάλογα τις δουλειές ή τις τεχνικές συζητήσεις...ή τα ποτά..)



pipeline {
    agent {
        kubernetes {
            yaml '''
apiVersion: v1
kind: Pod
spec:
  containers:
  - name: jnlp
    image: 'jenkins/inbound-agent'
    args: ['\$(JENKINS_SECRET)', '\$(JENKINS_NAME)']
  - name: hugo
    image: klakegg/hugo:debian
    command:
    - sleep
    args:
    - 1d
  - name: tea
    image: 'alpine'
    command:
    - sleep
    args:
    - 1d
'''
            defaultContainer 'hugo'
            workspaceVolume dynamicPVC(requestsSize: "10Gi")
        }
    }

    options {
        skipDefaultCheckout()
    }

    stages {
        stage('Grab Code') {
            steps {
                checkout scm
            }
        }

        stage('Test Hugo syntax') {
            steps {
                container('hugo') {
                    sh 'hugo --quiet'
                }
            }
        }

        stage('Build the Site') {
            steps {
                container('hugo') {
                    sh 'hugo'
                }
            }
        }

        stage('Create Package') {
            steps {
                container('hugo') {
                    sh "tar zcvf site.tar.gz ./public/"
                }
            }
        }

        stage('Upload Release') {
            environment {
                GITEA_LOGIN = 'js-hellug-bot'
                GITEA_SERVER_URL = 'https://dev.hellug.gr'
                GITEA_SERVER_TOKEN = credentials('jenkins-pipeline-bot-token')
            }

            steps {
                container('tea') {
                    sh 'ls -la && pwd && apk update && apk add --no-cache -X http://dl-cdn.alpinelinux.org/alpine/edge/testing tea'
                    sh "tea login add -n ${GITEA_LOGIN}"
                    sh "tea r create -t ${BUILD_TAG} --tag r${BUILD_NUMBER} -a site.tar.gz --repo hellug/www"
                }
            }
        }
    }
}
